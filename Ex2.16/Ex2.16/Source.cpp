
#include "SDL_Class.h"
#include "Globals.h"

std::vector<glm::vec2> controlPoints;
std::vector<glm::vec2> bezierCurve;

std::unique_ptr<SDL_Class> SDL;

//Bool controlling when to stop the program.
auto running = true;

//The clock time when the timer started
Uint32 ticks;
Uint32 lastTicks;
float dt;

auto mRight = true;
auto dotPos = 0.0;
glm::vec2 dot;

SDL_Texture* dotSprite;
SDL_Rect dotClip;

//SDL event handler.
SDL_Event event;

//The window we'll be rendering to
SDL_Window* gWindow = NULL;

//The SDL renderer.
SDL_Renderer* gRenderer = NULL;

//Starts up SDL and creates window
bool init();

bool loadMedia();

//Frees media and shuts down SDL
void close();


glm::vec2 calcBezier(std::vector<glm::vec2> controlP, float step);

bool init() {
	//Initialization flag
	bool success = true;

	//Initialize SDL
	if (SDL_Init(SDL_INIT_VIDEO) < 0) {
		printf("SDL could not initialize! SDL_Error: %s\n", SDL_GetError());
		success = false;
	}
	else {
		//Set texture filtering to linear
		if (!SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "1")) {
			printf("Warning: Linear texture filtering not enabled!");
		}

		//Create window
		gWindow = SDL_CreateWindow("Assignment 1", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH+1, SCREEN_HEIGHT+1, SDL_WINDOW_SHOWN);
		if (gWindow == NULL) {
			printf("Window could not be created! SDL_Error: %s\n", SDL_GetError());
			success = false;
		}
		else {

			//Create renderer for window
			gRenderer = SDL_CreateRenderer(gWindow, -1, SDL_RENDERER_ACCELERATED);
			if (gRenderer == NULL) {
				printf("Renderer could not be created! SDL Error: %s\n", SDL_GetError());
				success = false;
			}
			else {
				//Initialize renderer color
				SDL_SetRenderDrawColor(gRenderer, 0xFF, 0xFF, 0xFF, 0xFF);

				//Initialize PNG loading
				int imgFlags = IMG_INIT_PNG;
				if (!(IMG_Init(imgFlags) & imgFlags)) {
					printf("SDL_image could not initialize! SDL_mage Error: %s\n", IMG_GetError());
					success = false;
				}
			}
		}
	}

	return success;
}


bool loadMedia() {
	//Loading success flag
	bool success = true;

	SDL->FreeTexture(dotSprite);
	dotSprite = SDL->LoadTexture(DOTPATH, gRenderer);

	if (dotSprite == NULL) {
		std::cout << "Failed to load cell image";
		success = false;
	}

	dotClip.x = 0;
	dotClip.y = 0;
	dotClip.w = 32;
	dotClip.h = 32;

	SDL_Texture* spriteRender = NULL;

	return success;
}


void close() {

	//Destroy window
	SDL_DestroyWindow(gWindow);
	gWindow = NULL;

	//Destroy renderer
	SDL_DestroyRenderer(gRenderer);
	gRenderer = NULL;

	//Quit SDL subsystems
	IMG_Quit();
	SDL_Quit();
}


glm::vec2 calcBezier(std::vector<glm::vec2> controlP, float step) {
	float x = pow((1.0 - step), 3.0) * controlP.at(0).x 
			  + 3.0 * pow((1.0 - step), 2.0) * step * controlP.at(1).x 
			  + 3.0 * (1.0 - step) * pow(step, 2.0) * controlP.at(2).x
			  + pow(step, 3.0) * controlP.at(3).x;

	float y = pow((1.0 - step), 3.0) * controlP.at(0).y
			  + 3.0 * pow((1.0 - step), 2.0) * step * controlP.at(1).y
			  + 3.0 * (1.0 - step) * pow(step, 2.0) * controlP.at(2).y
			  + pow(step, 3.0) * controlP.at(3).y;
	
	return glm::vec2(x, y);
}



int main(int argc, char* args[]) {

	//Start up SDL and create window
	if (!init()) {
		printf("Failed to initialize!\n");
	} else {

		if (!loadMedia()) {
			printf("Failed to load media!\n");
		}

		//Get the current clock time
		ticks = SDL_GetTicks();
		lastTicks = ticks;

		//Initalize control points
		controlPoints.push_back(glm::vec2(0.0, 1.0));
		controlPoints.push_back(glm::vec2(2.0, 3.0));
		controlPoints.push_back(glm::vec2(3.0, 2.0));
		controlPoints.push_back(glm::vec2(5.0, 5.0));

		//Initalize bezier curve points
		for (float i = 0.0; i <= 1.0; i += 0.01) {
			bezierCurve.push_back(calcBezier(controlPoints, i));
		}


		while (running) {

			//Clear screen
			SDL_SetRenderDrawColor(gRenderer, 0x00, 0x00, 0x00, 0x00);
			SDL_RenderClear(gRenderer);


			//Check for input.
			while (SDL_PollEvent(&event)) {
				if (event.type == SDL_QUIT) {
					running = false;
				}
				if (event.type == SDL_KEYDOWN) {
					switch (event.key.keysym.sym) {
					case SDLK_ESCAPE:
						running = false;
						break;
					default:
						break;
					}
				}
			}

			//Set draw color white
			SDL_SetRenderDrawColor(gRenderer, 0xFF, 0xFF, 0xFF, 0xFF);
			//Draw center lines
			SDL_RenderDrawLine(gRenderer, SCREEN_WIDTH/2, 0, SCREEN_WIDTH/2, SCREEN_HEIGHT);
			SDL_RenderDrawLine(gRenderer, 0, SCREEN_HEIGHT / 2, SCREEN_WIDTH, SCREEN_HEIGHT / 2);

			//Draw horizontal steps
			for (int i = 0; i <= SCREEN_WIDTH; i += SCREEN_WIDTH / 20) {
				SDL_RenderDrawLine(gRenderer, i, (SCREEN_HEIGHT / 2) - 10, i, (SCREEN_HEIGHT / 2) + 10);
			}

			//Draw vertical steps
			for (int i = 0; i <= SCREEN_HEIGHT; i += SCREEN_HEIGHT / 20) {
				SDL_RenderDrawLine(gRenderer, (SCREEN_WIDTH / 2) - 10, i, (SCREEN_WIDTH / 2) + 10, i);
			}

			//Set draw color blue
			SDL_SetRenderDrawColor(gRenderer, 0x00, 0x00, 0xFF, 0xFF);

			//Draw lines between control points
			for (int i = 1; i < controlPoints.size(); i++) {
				int xStart = (controlPoints.at(i - 1).x * X_STEP) + CENTER_X;
				int yStart = -(controlPoints.at(i - 1).y * Y_STEP) + CENTER_Y;
				int xEnd = (controlPoints.at(i).x * X_STEP) + CENTER_X;
				int yEnd = -(controlPoints.at(i).y * Y_STEP) + CENTER_Y;
				SDL_RenderDrawLine(gRenderer, xStart, yStart, xEnd, yEnd);
			}

			//Set draw color green
			SDL_SetRenderDrawColor(gRenderer, 0x00, 0xFF, 0x00, 0xFF);

			//Draw bezier curve
			for (int i = 1; i < bezierCurve.size(); i++) {
				int xStart = (bezierCurve.at(i - 1).x * X_STEP) + CENTER_X;
				int yStart = -(bezierCurve.at(i - 1).y * Y_STEP) + CENTER_Y;
				int xEnd = (bezierCurve.at(i).x * X_STEP) + CENTER_X;
				int yEnd = -(bezierCurve.at(i).y * Y_STEP) + CENTER_Y;
				SDL_RenderDrawLine(gRenderer, xStart, yStart, xEnd, yEnd);
			}

			
			//Get ticks and calculate delta time
			ticks = SDL_GetTicks();
			dt = (ticks - lastTicks) / 1000.0;
			lastTicks = ticks;


			//Calculate dot position and render 
			dotPos += DOT_VEL * dt;
			dot = calcBezier(controlPoints, dotPos);
			SDL->Render(dot.x * X_STEP + CENTER_X - 16, -dot.y * Y_STEP + CENTER_Y - 16, dotSprite, dotClip, gRenderer);

			//When dot reaches end of line, put back to start.
			if (dotPos >= 1.0) {
				dotPos = 0.0;
			}


			//Update screen
			SDL_RenderPresent(gRenderer);

		}
	}


	//Free resources and close SDL
	close();

	return 0;
}