#pragma once


//Screen dimension constants
const auto SCREEN_WIDTH = 800;
const auto SCREEN_HEIGHT = 800;
const auto SCREEN_FPS = 60;
const auto SCREEN_TICKS_PER_FRAME = 1000 / SCREEN_FPS;

const auto CENTER_X = SCREEN_WIDTH / 2;
const auto CENTER_Y = SCREEN_HEIGHT / 2;

const auto X_STEP = SCREEN_WIDTH / 20;
const auto Y_STEP = SCREEN_HEIGHT / 20;

const auto DOT_VEL = 0.3;
const std::string DOTPATH = "Dot.png";