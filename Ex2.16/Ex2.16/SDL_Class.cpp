#include "SDL_Class.h"



SDL_Class::SDL_Class()
{
}


SDL_Class::~SDL_Class()
{
}


SDL_Texture* SDL_Class::LoadTexture(std::string path, SDL_Renderer* gRenderer) {

	//The final texture
	SDL_Texture* newTexture = NULL;

	//Load image at specified path
	SDL_Surface* loadedSurface = IMG_Load(path.c_str());
	if (loadedSurface == NULL) {
		printf("Unable to load image %s! SDL_image Error: %s\n", path.c_str(), IMG_GetError());
	}
	else {
		//Color key image
		SDL_SetColorKey(loadedSurface, SDL_TRUE, SDL_MapRGB(loadedSurface->format, 0, 0xFF, 0xFF));

		//Create texture from surface pixels
		newTexture = SDL_CreateTextureFromSurface(gRenderer, loadedSurface);
		if (newTexture == NULL) {
			printf("Unable to create texture from %s! SDL Error: %s\n", path.c_str(), SDL_GetError());
		}

		//Get rid of old loaded surface
		SDL_FreeSurface(loadedSurface);
	}

	//Return success
	return newTexture;
}

void SDL_Class::FreeTexture(SDL_Texture* tex) {
	if (tex != NULL) {
		SDL_DestroyTexture(tex);
		tex = NULL;
	}
}


void SDL_Class::Render(int x, int y, SDL_Texture* sprite, SDL_Rect clip, SDL_Renderer* gRenderer) {
	//Set rendering space and render to screen
	SDL_Rect renderQuad = { x, y, clip.w, clip.h };

	//Render to screen
	SDL_RenderCopy(gRenderer, sprite, &clip, &renderQuad);
}