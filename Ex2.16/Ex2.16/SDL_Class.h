#pragma once

#include "Includes.h"

class SDL_Class
{
public:
	SDL_Class();
	~SDL_Class();
	SDL_Texture* LoadTexture(std::string path, SDL_Renderer* gRenderer);
	void FreeTexture(SDL_Texture* tex);
	void Render(int x, int y, SDL_Texture* sprite, SDL_Rect clip, SDL_Renderer* gRenderer);
};

