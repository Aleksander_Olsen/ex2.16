#pragma once


#include <gl\glew.h>
#include <gl\glu.h>

#define GLM_FORCE_RADIANS //Using degrees with glm is deprecated.
#include <glm/glm.hpp>
#include <glm/gtc/constants.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <SDL.h>
#include <SDL_image.h>

#include <iostream>
#include <vector>
#include <memory>