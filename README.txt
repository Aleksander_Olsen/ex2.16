
HOW TO MAKE PROJECT WORK IN VISUAL STUDIO 2015

1. Unzip SDL2.zip.
2. Put the folder SDL2 in the first folder named "Ex2.16"

1. Unzip SDL_DLLs.zip.
2. Put dll files in ..\Ex2.16\Ex2.16\Debug after first build.

1. Find glew32.dll in folder ..\GLEW\glew-1.13.0\bin\Release\win32
2. Put dll in same place as above.

Add include libraries:
- Go to project settings -> C/C++ -> general -> Additional Include Directories -> edit
- add "..\SDL2\SDL2-2.0.3vs15\include"
- add "..\SDL2\SDL2_image-2.0.0\include"
- add "..\GLEW\glew-1.13.0\include"

Add library:
- Go to project settings -> Linker -> Additional Library Directories -> edit
- add "..\SDL2\SDL2-2.0.3vs15\lib\x86"
- add "..\SDL2\SDL2_image-2.0.0\lib\x86"
- add "..\GLEW\glew-1.13.0\lib\Release\Win32"

- Go to Linker -> Input -> Additional Dependencies ->edit
- add SDL2.lib, SDL2main.lib, SDL2_image.lib, glew32.lib

- Go to Linker -> System
- SubSystem sould be set to Console.




Author: Aleksander Olsen.

